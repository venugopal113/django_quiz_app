## Project Description

-- This project is made with Django FrameWork.
-- This app is a simple quiz app where:
	-- logged in users can add quiz with multiple choice questions.
	-- any user can take the quiz and get his score.
	-- quizes has been categorized.

### Installation

-- clone the git repo
-- create a virtual environment in python3
-- create a superuser
`bash
pip install -r requirements.txt
`
-- run manage.py
-- listen on localhost:8000 port in your browser to get it working.


