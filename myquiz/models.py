from django.db import models
from django.utils import timezone
from django.conf import settings


class Quiz(models.Model):
    quiz_title = models.CharField(max_length=200)
    quiz_category = models.CharField(max_length=100)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.quiz_title


class Question(models.Model):
    quiz_id = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    body = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.body


class Choice(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    body = models.CharField(max_length=50)

    def __str__(self):
        return self.body


class CorrectChoice(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    chocie_id = models.ForeignKey(Choice, on_delete=models.CASCADE)


class QuestionAttempt(models.Model):
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    is_ans_correct = models.BooleanField(default=False)
