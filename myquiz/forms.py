from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Question, Quiz, Choice


class AddQuizForm(forms.ModelForm):

    class Meta:
        model = Quiz
        fields = ('quiz_title','quiz_category')
        labels = {'quiz_title':'Title', 'quiz_category':'Category'}


class AddQuestionsForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ('body',)
        labels = {'body':'Question'}


class AddChoiceForm(forms.Form):
    choice1 = forms.CharField(max_length=50, help_text='Choice1')
    choice2 = forms.CharField(max_length=50, help_text='Choice2')
    choice3 = forms.CharField(max_length=50, help_text='Choice3')
    choice4 = forms.CharField(max_length=50, help_text='Choice4')
    correct_choice = forms.CharField(max_length=50, help_text='Please enter correct choice among above choices')


class SignUpForm(UserCreationForm):
    full_name = forms.CharField(max_length=50, help_text='Please enter your full name')
    email = forms.EmailField(max_length=254, help_text='Required, Please provide a valid email')

    class Meta:
        model = User
        fields = ('username', 'full_name', 'email', 'password1', 'password2',)
