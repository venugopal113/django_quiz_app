from django.contrib.auth import views as auth_views
from django.urls import path

from . import views


urlpatterns = [
    path('', views.quiz_list, name='home'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', views.signup, name='signup'),
    path('quiz/new/', views.quiz_new, name='new_quiz'),
    path('quiz/<int:pk>/', views.quiz_detail, name='quiz_detail'),
    path('quiz/<int:pk>/question_new', views.question_new, name='question_new'),
    path('quiz/<int:quiz_pk>/question/<int:question_pk>', views.choices, name='add_choice'),
]
