from django.conf import settings
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.contrib.auth import login, authenticate
from django.contrib import messages

from .forms import AddQuizForm, AddChoiceForm, AddQuestionsForm, SignUpForm
from .models import Quiz, Question, Choice, CorrectChoice, QuestionAttempt


def quiz_list(request):
    quiz_items = Quiz.objects.filter(
        created_on__lte=timezone.now()).order_by('-created_on')
    return render(request, 'quiz/quiz_list.html', {'quiz_items': quiz_items})


def quiz_new(request):
    if request.method == 'POST':
        quiz_form = AddQuizForm(request.POST)
        if quiz_form.is_valid():
            quiz = quiz_form.save(commit=False)
            quiz.created_by = request.user
            quiz.save()
            return redirect('quiz_detail', pk=quiz.pk)
    else:
        if request.user.is_authenticated:
            quiz_form = AddQuizForm()
        else:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    return render(request, 'quiz/quiz_new.html', {'quiz_form': quiz_form})


score = 0


def quiz_detail(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    questions = Question.objects.filter(quiz_id=pk).order_by('-created_on')
    choices = []
    for question in questions:
        choices.append(
            {question: Choice.objects.filter(question_id=question)})
    if quiz.created_by == request.user:
        return render(request, 'quiz/quiz_detail.html', {'quiz': quiz, 'choices': choices})
    else:
        if request.user.is_authenticated:
            user = request.user.username
        else:
            user = 'Guest'
        if request.method == "POST":
            global score
            for question in questions:
                if QuestionAttempt.objects.filter(question_id=question, user_id=request.user).first():
                    question_attempt = QuestionAttempt.objects.filter(question_id=question, user_id=request.user).first()
                else:
                    question_attempt = QuestionAttempt(question_id = question, user_id = request.user)
                correct_ans = CorrectChoice.objects.filter(question_id=question.id).first()
                correct_ans_id = Choice.objects.filter(id=correct_ans.chocie_id.id).first()
                if request.POST[question.body] == correct_ans_id.body:
                    question_attempt.is_ans_correct = True
                else:
                    question_attempt.is_ans_correct = False
                question_attempt.save()
                if QuestionAttempt.objects.filter(question_id=question, user_id=request.user).first().is_ans_correct:
                    score += 1
            messages.info(request, "You have answered {} out of {} correctly".format(score,len(questions)))
            return redirect('quiz_detail', pk=pk)
        else:
            return render(request, 'quiz/take_quiz.html', {'user': user, 'quiz':quiz, 'choices':choices})


def question_new(request, pk):
    if request.method == 'POST':
        question_form = AddQuestionsForm(request.POST)
        if question_form.is_valid():
            question = question_form.save(commit=False)
            question.quiz_id = Quiz.objects.filter(id=pk).first()
            question.save()
            return redirect('quiz_detail', pk=pk)
    else:
        if request.user.is_authenticated:
            question_form = AddQuestionsForm()
        else:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    return render(request, 'quiz/questions_new.html', {'question_form': question_form})


def choices(request, quiz_pk, question_pk):
    question = Question.objects.filter(id=question_pk).first()
    if request.method == 'POST':
        choice_form = AddChoiceForm(request.POST)
        question = get_object_or_404(Question, id=question_pk)
        choice_id = None
        if choice_form.is_valid():
            for i in range(1, 5):
                choice = Choice(body=request.POST["{}{}".format(
                    'choice', i)], question_id=question)
                choice.save()
                if request.POST['correct_choice'] == request.POST["{}{}".format('choice', i)]:
                    choice_id = choice.id
            choice = get_object_or_404(Choice, id=choice_id)
            correctchoice = CorrectChoice(
                question_id=question, chocie_id=choice)
            correctchoice.save()
            return redirect('quiz_detail', pk=quiz_pk)
    else:
        if request.user.is_authenticated:
            choice_form = AddChoiceForm()
        else:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    return render(request, 'quiz/add_choices.html', {'choice_form': choice_form,'question':question})


def signup(request):
    if request.method == 'POST':
        sign_up_form = SignUpForm(request.POST)
        if sign_up_form.is_valid():
            sign_up_form.save()
            username = sign_up_form.cleaned_data.get('username')
            raw_password = sign_up_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        sign_up_form = SignUpForm()
    return render(request, 'signup.html', {'sign_up_form': sign_up_form})
